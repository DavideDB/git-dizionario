## Dizionario GIT 
 
 
 
Clonare un repository 
```
 git clone ...
```


Aggiungere un file
```
 git add nomefile
```

Aggiungere tutto il contenuto della cartella 
```
 git add *
```

Committare 
```
 git commit -am "Cosa ho modificato"
```

Pushare su branch
```
 git push origin nomebranch
```

Creare un nuovo branch
```
 git checkout -b nomebranch
```

Cambiare branch
```
 git checkout nomebranch
```

Vedere i branch disponibili e in che branch siamo 
```
 git branch
```

Revertare un commit
```
 git revert commithash
```
